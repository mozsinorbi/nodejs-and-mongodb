const supertest = require('supertest')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const app = require('../src/app')
const User = require('../src/db/models/user')

const userOneId = new mongoose.Types.ObjectId()

const userOne = {
    _id: userOneId,
    name: 'Test',
    email: 'test@test.com',
    password: 'testtest',
    tokens:[{
        token: jwt.sign({_id: userOneId}, process.env.JWT_SECRET)
    }]
}

beforeEach(async () => {
    await User.deleteMany()
    await User(userOne).save()
})

test ('Should signup a new user', async () => {
    const response = await supertest(app).post('/users').send({
        name:'Joska',
        email: 'aadas@yahoo.com',
        password: 'norbinorbi'
    }).expect(201)

    // asert that the database was changed correctly
    const user = await User.findById(response.body.user._id)
    expect(user).not.toBeNull()

    // Assertions about the response
    expect(response.body).toMatchObject({
        user:{
            name: 'Joska',
            email: 'aadas@yahoo.com'
        },
        token: user.tokens[0].token
    })
    expect(user.password).not.toBe('password')
})

test ('Should login existing user', async () => {
    const response = await supertest(app).post('/users/login').send({
        email: userOne.email,
        password: userOne.password
    }).expect(200)

    expect(userOne.tokens[1]).toBe(response.token)

})

test ('Should login non existing user', async () => {
    await supertest(app).post('/users/login').send({
        email: userOne.password,
        password: 'fake'
    }).expect(400)
})

test ('Should get profile for user', async () => {
    await supertest(app)
    .get('/users/me')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send()
    .expect(200)
})

test ('Should not get profile for unauthenticated user', async () => {
    await supertest(app)
    .get('/users/me')
    // .set('Authorization', `Bearer fsdfsfds`)
    .send()
    .expect(401)
})

test ('Should delete profile for authenticated user', async () => {
    await supertest(app)
    .delete('/users/me')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send()
    .expect(200)

    const user = await User.findById(userOneId)
    expect(user).not.toBe()
})

test ('Should not delete profile for nonauthenticated user', async () => {
    await supertest(app)
    .delete('/users/me')
    // .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send()
    .expect(401)
})

test('Shoul upload avatar image', async () => {
    await supertest(app)
    .post('/users/me/avatar')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .attach('joska','tests/fixtures/profile-pic.jpg') //joska, ahogy volt kuldve postmanbol

    const user = await User.findById(userOneId)
    expect(user.avatar).toEqual(expect.any(Buffer))
})

test('Should update valid user fields', async () => {
    await supertest(app)
    .patch('/users/me/')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send({
        name: 'Update'
    })
    .expect(200)
    
    const user = await User.findById(userOneId)
    expect(user.name).toEqual('Update')
})

test('Should not update invalid user fields', async () => {
    await supertest(app)
    .patch('/users/me/')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send({
        location: 'Update'
    })
    .expect(400)
    
    //expect(userOne.name).toBe('Update')
})