const User = require('../db/models/user')
    sharp = require('sharp')
const {sendWelcomeEmail} = require('../emails/account')
const {sendCancelEmail} = require('../emails/account')

const getAllUsers = (async (req, res) => {
  try {
    const users = await User.find({})
    res.send(users)
} catch (e) {
    res.status(500).send()
}
})

const getMe = (async (req, res) => {
    res.send(req.user)
})

const createUser = (async (req, res) => {
  const user = new User(req.body)
    try {
        await user.save()
        sendWelcomeEmail(user.email, user.name)
        const token = await user.generateAuthToken()
        
        res.status(201).send({user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})

const updateUserById = (async (req, res) => {
  const updates = Object.keys(req.body)
    const allowedUpdates = ['name', 'email', 'password', 'age']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid updates!' })
    }
    try {
        updates.forEach((update) => {req.user[update] = req.body[update]})
        await req.user.save()
        res.send(req.user)
    } catch (e) {
        res.status(400).send(e)
    }
})

const deleteUserById = (async (req, res) => {
  try {
    //const user = await User.findByIdAndDelete(req.user._id)
    req.user.remove()
    sendCancelEmail(req.user.email, req.user.name)
    res.send(req.user)
} catch (e) {
    res.status(500).send()
}
})

const loginUser = (async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({user, token})
    } catch (error) {
        res.status(400).send(error.message)
    }
})

const logoutUser = (async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((object) => {
            return object.token !== req.token
        })
        await req.user.save()

        res.send()
    } catch (error) {
        res.status(500).send(error.message)
    }
})

const logoutAll = (async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send()
    } catch (error) {
        res.status(500).send(error.message)
    }
})

const uploadAvatar = ( async (req, res) => {

    const buffer = await sharp(req.file.buffer)
    .resize({width: 250, height: 250}).png().toBuffer()

    req.user.avatar = buffer
    await req.user.save()
    res.send()
})

const deleteAvatar = ( async (req, res) => {
    req.user.avatar = undefined
    await req.user.save()
    res.send()
})

const getAvatarById = ( async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
        //console.log(user.avatar)
        if(!user || !user.avatar){
            throw new Error("the user or the avatar does not exists")
        }
        //res.contentType('image/jpeg');
        res.set('Content-Type','image/png');
        res.send(user.avatar)
    } catch (error) {
        res.status(400).send({error: error.message})
    }
})

  module.exports = {
    getAllUsers,
    createUser,
    updateUserById,
    deleteUserById,
    loginUser,
    logoutUser,
    logoutAll,
    getMe,
    uploadAvatar,
    deleteAvatar,
    getAvatarById
  };
  