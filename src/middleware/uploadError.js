const uploadError = (error, req, res, next) => {
    res.status(400).send({error: error.message}) // if get error at uploads
}
module.exports = uploadError