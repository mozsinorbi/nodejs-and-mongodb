const express = require('express'),
  controller = require('../controller/task')
  auth = require('../middleware/auth')

const router = express.Router();

router.route('/tasks')
  .get(auth, controller.getAllTasks)
  .post(auth, controller.createTask)

   router.route('/tasks/:id')
   .get(auth, controller.getTaskById)
   .patch(auth, controller.updateTaskById)
   .delete(auth, controller.deleteTaskById)

module.exports = router;
