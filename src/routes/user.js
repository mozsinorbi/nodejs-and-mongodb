const express = require('express'),
  controller = require('../controller/user'),
  auth = require('../middleware/auth'),
  uploadError = require('../middleware/uploadError'),
  multer = require('multer')
  upload = multer({
      limits: {
        fileSize: 1000000 // number in bytes
      },
      fileFilter(req, file, cb) {
        if (! file.originalname.match(/\.(jpg|jpeg|png)$/)){
          return cb(new Error('file must be jpg | jpeg | png'))
          // or return cb(undefined, false)
        }
         cb(undefined, true) // if a pdf (if correct the data)
      }
  })
const router = express.Router();

router.route('/users')
  .get(controller.getAllUsers)
  .post(controller.createUser)

  router.route('/users/login')
  .post(controller.loginUser)

  router.route('/users/logout')
  .post(auth, controller.logoutUser)

  router.route('/users/logoutAll')
  .post(auth, controller.logoutAll) 

  router.route('/users/:id/avatar')
  .get(controller.getAvatarById) 

  router.route('/users/me')
  .get(auth, controller.getMe)
  .delete(auth, controller.deleteUserById)
  .patch(auth, controller.updateUserById)
  
  router.route('/users/me/avatar')
   .post(auth, upload.single('joska'), uploadError, controller.uploadAvatar )
   .delete(auth, controller.deleteAvatar)

module.exports = router
