FROM node:13.4.0

WORKDIR /usr/src/app

ENV SENDGRID_API_KEY=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
ENV MONGODB_URL='mongodb+srv://taskapp:taskapp@cluster0-pvk9s.mongodb.net/task-manager-api?retryWrites=true&w=majority'
ENV JWT_SECRET=mozeskoss
ENV PORT=3000

COPY package*.json ./

RUN npm install
# RUN rm -rf node_modules/sharp
# RUN npm install --arch=x64 --platform=linux --target=8.10.0 sharp

COPY . .

# Port megjelölése (szemantikus)
EXPOSE 3000

# Indító parancsa a konténernek
# Ez a parancs NEM fut buildeléskor le, hanem csak az innen létrejött
#   konténer indításakor
CMD ["npm", "start"]

